import Swiper, { Navigation } from 'swiper'
const arrSTeam = document.querySelectorAll('.s-team')

arrSTeam.forEach(sTeam => {
	let carousel = sTeam.querySelector('.swiper')
	let prevEl = sTeam.querySelector('.swiper-button-prev')
	let nextEl = sTeam.querySelector('.swiper-button-next')
	const swiper = new Swiper(carousel, {
		modules: [Navigation],
		spaceBetween: 20,
		navigation: {
			nextEl,
			prevEl
		},
		breakpoints: {
			320: {
				slidesPerView: 1.1,
				spaceBetween: 10
			},
			768: {
				slidesPerView: 2.3,
				spaceBetween: 20
			},
			992: {
				slidesPerView: 3.66,
				spaceBetween: 30
			}
		}
	})
})
