import Swiper, { Navigation } from 'swiper'
const arrSPreview = document.querySelectorAll('.s-preview')

arrSPreview.forEach(sPreview => {
	let carousel = sPreview.querySelector('.swiper')
	let prevEl = sPreview.querySelector('.swiper-button-prev')
	let nextEl = sPreview.querySelector('.swiper-button-next')
	const swiper = new Swiper(carousel, {
		modules: [Navigation],
		spaceBetween: 20,
		navigation: {
			nextEl,
			prevEl
		},
		breakpoints: {
			320: {
				slidesPerView: 1.2,
				spaceBetween: 20
			},
			768: {
				slidesPerView: 1.5,
				spaceBetween: 30
			},
			992: {
				slidesPerView: 2.6,
				spaceBetween: 30
			}
		}
	})
})
