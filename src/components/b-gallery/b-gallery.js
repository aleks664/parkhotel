const arrSliderGallery = document.querySelectorAll('.b-gallery_has-slider')
import Swiper from 'swiper'
import { breakpoints } from '../../base/js/core'
arrSliderGallery.forEach(gallery => {
	let carousel = gallery.querySelector('.swiper')
	let options = {
		loop: true,
		slidesPerView: 1.2,
		spaceBetween: 10
	}
	let swiper
	const swiperInit = () => {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
		if (width >= breakpoints.tablet) {
			if (carousel.classList.contains('swiper-initialized')) {
				swiper.destroy()
			}
		} else if (!carousel.classList.contains('swiper-initialized')) {
			swiper = new Swiper(carousel, options)
		}
	}
	swiperInit()
	window.addEventListener('resize', swiperInit)
})
