const openMenu = document.querySelector('.js-open-menu')
const closeMenu = document.querySelector('.js-close-menu')
const mainManu = document.getElementById('mainMenu')
openMenu.addEventListener('click', () => {
	document.documentElement.classList.add('is-main-menu-open')
	mainManu.classList.add('is-open')
})
closeMenu.addEventListener('click', () => {
	document.documentElement.classList.remove('is-main-menu-open')
	mainManu.classList.remove('is-open')
})
