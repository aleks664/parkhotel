import './s-header/s-header'
import './s-promo/s-promo'
import './s-about/s-about'
import './b-carousel-cards/b-carousel-cards'
import './s-preview/s-preview'
import './s-preview-tile/s-preview-tile'
import './s-popup/s-popup'
import './s-filter/s-filter'
import './card-room/card-room'
import './s-other-rooms/s-other-rooms'
import './s-team/s-team'
import './b-gallery/b-gallery'
import './s-stocks/s-stocks'
import './policy-box/policy-box'

