const btnMore = document.querySelector('.policy-box__more a')
const block = document.querySelector('.policy-box')
if (btnMore) {
	btnMore.addEventListener('click', e => {
		let txt = block.classList.contains('is-open') ? 'Показать ещё' : 'Скрыть'
		block.classList.toggle('is-open')
		btnMore.textContent = txt
		e.preventDefault()
	})
}
