import Swiper, { Navigation, Pagination } from 'swiper'
const arrAbout = document.querySelectorAll('.s-about')
const count = (el, swiper) => {
	el.textContent = `${swiper.realIndex + 1}/${swiper.slides.length}`
}
arrAbout.forEach(about => {
	let prevEl = about.querySelector('.swiper-button-prev')
	let nextEl = about.querySelector('.swiper-button-next')
	let slider = about.querySelector('.swiper')
	let pagination = about.querySelector('.swiper-pagination')
	let countEl = about.querySelector('.s-about__count')
	const swiper = new Swiper(slider, {
		modules: [Navigation, Pagination],
		autoHeight: true,
		loop: true,
		spaceBetween: 0,
		pagination: {
			el: pagination,
			type: 'progressbar'
		},
		navigation: {
			nextEl,
			prevEl
		},
		on: {
			init(sw) {
				count(countEl, sw)
			},
			slideChange(sw) {
				count(countEl, sw)
			}
		}
	})
})
