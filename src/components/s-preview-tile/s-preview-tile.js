import Swiper from 'swiper'
import { breakpoints } from '../../base/js/core'
const arrSPreview = document.querySelectorAll('.s-preview-tile')
arrSPreview.forEach(sPreview => {
	let carousel = sPreview.querySelector('.swiper')
	let options = {
		slidesPerView: 2.5,
		spaceBetween: 15,
		breakpoints: {
			320: {
				slidesPerView: 2.5,
				spaceBetween: 15
			},
			768: {
				slidesPerView: 3.5,
				spaceBetween: 20
			}
		}
	}
	let swiper
	const swiperInit = () => {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
		if (width >= breakpoints.desktop) {
			if (carousel.classList.contains('swiper-initialized')) {
				swiper.destroy()
			}
		} else if (!carousel.classList.contains('swiper-initialized')) {
			swiper = new Swiper(carousel, options)
		}
	}
	swiperInit()
	window.addEventListener('resize', swiperInit)
})
