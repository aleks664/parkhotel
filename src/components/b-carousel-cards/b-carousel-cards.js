import Swiper from 'swiper'
const arrCarCards = document.querySelectorAll('.b-carousel-cards')

arrCarCards.forEach(line => {
	let carousel = line.querySelector('.swiper')
	const swiper = new Swiper(carousel, {
		spaceBetween: 15,
		breakpoints: {
			320: {
				slidesPerView: 2.5,
				spaceBetween: 15
			},
			768: {
				slidesPerView: 3.5,
				spaceBetween: 20
			},
			992: {
				slidesPerView: 5,
				spaceBetween: 20
			},
			1200: {
				slidesPerView: 6,
				spaceBetween: 20
			}
		}
	})
})
