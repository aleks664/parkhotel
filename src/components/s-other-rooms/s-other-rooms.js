import Swiper, { Navigation } from 'swiper'
const arrSOutherRooms = document.querySelectorAll('.s-outher-rooms')

arrSOutherRooms.forEach(sOtherRooms => {
	let carousel = sOtherRooms.querySelector('.swiper')
	let prevEl = sOtherRooms.querySelector('.swiper-button-prev')
	let nextEl = sOtherRooms.querySelector('.swiper-button-next')
	const swiper = new Swiper(carousel, {
		modules: [Navigation],
		spaceBetween: 20,
		navigation: {
			nextEl,
			prevEl
		},
		breakpoints: {
			320: {
				slidesPerView: 1.2,
				spaceBetween: 15
			},
			768: {
				slidesPerView: 1.5,
				spaceBetween: 30
			},
			992: {
				slidesPerView: 2.66,
				spaceBetween: 30
			}
		}
	})
})
