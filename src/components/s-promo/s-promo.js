import Swiper, { Navigation } from 'swiper'
const arrPromo = document.querySelectorAll('.s-promo')
arrPromo.forEach(promo => {
	let prevEl = promo.querySelector('.swiper-button-prev')
	let nextEl = promo.querySelector('.swiper-button-next')
	let slider = promo.querySelector('.swiper')
	const swiper = new Swiper(slider, {
		modules: [Navigation],
		autoHeight: true,
		spaceBetween: 0,
		navigation: {
			nextEl,
			prevEl
		}
	})
})
