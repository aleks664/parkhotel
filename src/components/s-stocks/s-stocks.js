import { breakpoints } from '../../base/js/core'

const arrStock = document.querySelectorAll('.s-stocks')
const toogleCardResize = (arr, btn) => {
	const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
	arr.forEach((card, i) => {
		if (i > 4 && width < breakpoints.tablet) {
			card.style.display = 'none'
			btn.classList.remove('is-active')
			btn.textContent = 'Показать еще'
		} else if (width >= breakpoints.tablet) {
			card.style.display = 'block'
		}
	})
}
arrStock.forEach(stock => {
	let arrCard = stock.querySelectorAll('.s-stocks__itm')
	let btn = stock.querySelector('.s-stocks__more a')
	if (btn) {
		btn.addEventListener('click', e => {
			e.preventDefault()
			btn.classList.toggle('is-active')
			let txt = !btn.classList.contains('is-active') ? 'Показать еще' : 'Сркыть'
			btn.textContent = txt
			arrCard.forEach((card, i) => {
				if (i > 4) {
					if (btn.classList.contains('is-active')) {
						card.style.display = 'block'
					} else {
						card.style.display = 'none'
					}
				}
			})
		})
		if (arrCard.length > 5) {
			toogleCardResize(arrCard, btn)
			window.addEventListener('resize', () => {
				toogleCardResize(arrCard, btn)
			})
		}
	}
})

