import { slideToggle } from '../../base/js/core'

const arrBtnFilter = document.querySelectorAll('[data-filter-toggle]')
arrBtnFilter.forEach(btn => {
	let filter = document.getElementById(btn.dataset.filterToggle.substring(1))
	if (filter) {
		btn.addEventListener('click', e => {
			e.preventDefault()
			let txt = !btn.classList.contains('is-open') ? 'Открыть фильтр' : 'Скрыть фильтр'

			btn.classList.toggle('is-open')
			btn.querySelector('span').textContent = txt
			slideToggle(filter)
		})
	}
})
