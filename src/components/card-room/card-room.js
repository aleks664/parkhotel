import Swiper, { Navigation } from 'swiper'

const arrCardRoom = document.querySelectorAll('.card-room')
arrCardRoom.forEach(room => {
	let prevEl = room.querySelector('.swiper-button-prev')
	let nextEl = room.querySelector('.swiper-button-next')
	let slider = room.querySelector('.swiper')
	const swiper = new Swiper(slider, {
		modules: [Navigation],
		loop: true,
		spaceBetween: 0,
		navigation: {
			nextEl,
			prevEl
		}
	})
})
